﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace showCommanderWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private int time;
        private bool cancel = false;

        private void SetButtonStopColor(Color color)
        {
            buttonStop.Background = new SolidColorBrush(color);
            buttonStop.BorderBrush = new SolidColorBrush(color);
            this.Background = new SolidColorBrush(color);
        }

        private void RunTimer()
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                DisableGo();
            }));
            for (int i = time; i > 0; i--)
            {
                if (!cancel)
                {
                    Dispatcher.BeginInvoke((Action)(() =>
                        {
                            buttonStop.Content = i.ToString();
                            if (i > 10)
                            {
                                SetButtonStopColor(Color.FromRgb(64, 64, 64));
                            }
                            else if (i > 5 && i < 11)
                            {
                                SetButtonStopColor(Color.FromRgb(192, 128, 0));
                            }
                            else
                            {
                                SetButtonStopColor(Color.FromRgb(192, 0, 0));
                            }
                        }));

                    Thread.Sleep(1000);
                }
            }
            if (!cancel)
            {
                FlashTimer();
                Dispatcher.BeginInvoke((Action)(() =>
                {
                    EnableGo();
                }));
            }
        }

        private void DisableGo()
        {
            buttonGoBack.IsEnabled = false;
            buttonGoForward.IsEnabled = false;
            buttonGo5.IsEnabled = false;
            buttonGo10.IsEnabled = false;
            buttonGo15.IsEnabled = false;
            buttonGo20.IsEnabled = false;
            buttonGo25.IsEnabled = false;
            buttonGo30.IsEnabled = false;
        }

        private void EnableGo()
        {
            buttonGoBack.IsEnabled = true;
            buttonGoForward.IsEnabled = true;
            buttonGo5.IsEnabled = true;
            buttonGo10.IsEnabled = true;
            buttonGo15.IsEnabled = true;
            buttonGo20.IsEnabled = true;
            buttonGo25.IsEnabled = true;
            buttonGo30.IsEnabled = true;
        }

        private void FlashTimer()
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                buttonStop.Content = "GO";
                SetButtonStopColor(Color.FromRgb(0, 192, 0));
                canvasCurrent.Background = new SolidColorBrush(Color.FromRgb(0, 192, 0));

            }));
            Thread.Sleep(250);
            Dispatcher.BeginInvoke((Action)(() =>
            {
                SetButtonStopColor(Color.FromRgb(192, 0, 0));
                canvasCurrent.Background = new SolidColorBrush(Color.FromRgb(192, 0, 0));
            }));
            Thread.Sleep(250);
            Dispatcher.BeginInvoke((Action)(() =>
            {
                SetButtonStopColor(Color.FromRgb(0, 192, 0));
                canvasCurrent.Background = new SolidColorBrush(Color.FromRgb(0, 192, 0));
            }));
            Thread.Sleep(250);
            Dispatcher.BeginInvoke((Action)(() =>
            {
                SetButtonStopColor(Color.FromRgb(192, 0, 0));
                canvasCurrent.Background = new SolidColorBrush(Color.FromRgb(192, 0, 0));
            }));
            Thread.Sleep(250);
            Dispatcher.BeginInvoke((Action)(() =>
            {
                SetButtonStopColor(Color.FromRgb(0, 192, 0));
                canvasCurrent.Background = new SolidColorBrush(Color.FromRgb(0, 192, 0));
            }));
            Thread.Sleep(250);
            Dispatcher.BeginInvoke((Action)(() =>
            {
                ResetTimer();
            }));
        }
        private void ResetTimer()
        {
            SetButtonStopColor(Color.FromRgb(0, 0, 0));
            canvasCurrent.Background = new SolidColorBrush(Color.FromRgb(0, 64, 0));
            this.Background = new SolidColorBrush(Color.FromRgb(32, 32, 32));
            buttonStop.Content = "TIME";
        }

        private void ButtonQuit_Click(object sender, RoutedEventArgs e)
        {
            cancel = true;
            Thread.Sleep(250);
            Close();
        }

        private void ButtonGo5_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            time = 5;
            Thread timerThread = new Thread(new ThreadStart(RunTimer));
            timerThread.Start();
        }

        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            cancel = true;
            EnableGo();
            ResetTimer();
        }

        private void ButtonGo10_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            time = 10;
            Thread timerThread = new Thread(new ThreadStart(RunTimer));
            timerThread.Start();
        }

        private void ButtonGo15_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            time = 15;
            Thread timerThread = new Thread(new ThreadStart(RunTimer));
            timerThread.Start();
        }

        private void ButtonGo20_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            time = 20;
            Thread timerThread = new Thread(new ThreadStart(RunTimer));
            timerThread.Start();
        }

        private void ButtonGo25_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            time = 25;
            Thread timerThread = new Thread(new ThreadStart(RunTimer));
            timerThread.Start();
        }

        private void ButtonGo30_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            time = 30;
            Thread timerThread = new Thread(new ThreadStart(RunTimer));
            timerThread.Start();
        }

        private void ButtonGoForward_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            time = 0;
            Thread timerThread = new Thread(new ThreadStart(RunTimer));
            timerThread.Start();
        }

        private void ButtonGoBack_Click(object sender, RoutedEventArgs e)
        {
            cancel = false;
            time = 0;
            Thread timerThread = new Thread(new ThreadStart(RunTimer));
            timerThread.Start();
        }
    }
}
